(use extras)

(define contatos '())

(define arquivo-de-contatos "contatos.txt")

(define (salva-contatos)
  (let ([porta (open-output-file arquivo-de-contatos)])
    (write contatos porta)
    (close-output-port porta)))

(define (carrega-contatos)
  (cond [(file-exists? arquivo-de-contatos)
	 (let ([porta (open-input-file arquivo-de-contatos)])
	   (set! contatos (read porta))
	   (close-input-port porta))]
	[else
	 (void)]))

(define (lê-string prompt)
  (display prompt)
  (read-line))

(define (lê-número prompt)
  (string->number (lê-string prompt)))

(define (menu)
  (display "=== Menu ===
  (1) Inserir contato
  (2) Listar todos os contatos
  (3) Procurar contato por nome
  (4) Remover contato
  (5) Sair\n")
  (let ([opção (lê-número "Digite uma opção: ")])
    (cond [(= opção 1) (inserir-contato) #t]
	  [(= opção 2) (listar-contatos) #t]
	  [(= opção 3) (procurar-contato) #t]
	  [(= opção 4) (remover-contato) #t]
	  [(= opção 5) #f]
	  [else (display "Opção inválida!\n") #t])))

(define (main-loop)
  (cond [(menu) (main-loop)]
	[else (void)]))

(define (novo-contato nome telefone)
  (list nome telefone))

(define (nome-do-contato contato)
  (car contato))

(define (telefone-do-contato contato)
  (cadr contato))

(define (inserir-contato)
  (define nome (lê-string "Nome: "))
  (define telefone (lê-string "Telefone: "))
  (set! contatos (cons (novo-contato nome telefone) contatos)))

(define (imprime-contato contato)
  (printf "Nome: ~a\n" (nome-do-contato contato))
  (printf "Telefone: ~a\n" (telefone-do-contato contato))
  (printf "\n"))

(define (imprime-contatos lst)
  (cond
   [(null? lst) (void)]
   [else (imprime-contato (car lst))
	 (imprime-contatos (cdr lst))]))

(define (listar-contatos)
  (imprime-contatos contatos))

(define (procura-contato nome contatos) ;melhorar nome da função
  (cond
   [(null? contatos)
    #f]
   [(string=? nome (nome-do-contato (car contatos))) ;quebrar linha aqui
    (car contatos)]
   [else (procura-contato nome (cdr contatos))]))

(define (procurar-contato)
  (define nome (lê-string "Nome procurado: "))
  (define contato (procura-contato nome contatos))
  (cond
   [contato (imprime-contato contato)]
   [else (display "Contato não encontrado!\n")]))

(define (remove-contato nome contatos)
  (cond
   [(null? contatos) contatos]
   [(string=? nome (nome-do-contato (car contatos)))
    (cdr contatos)]
   [else (cons (car contatos)
	       (remove-contato nome (cdr contatos)))]))

(define (remover-contato)
  (define nome (lê-string "Nome a remover: "))
  (set! contatos (remove-contato nome contatos)))

(carrega-contatos)
(main-loop)
(salva-contatos)
