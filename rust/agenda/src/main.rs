use std::io;
use std::io::prelude::*;
use std::io::BufReader;
use std::fs::File;
use std::cell::RefCell;
use std::str::FromStr;

struct Contato {
    nome: String,
    telefone: String,
}

const FILE_NAME: &'static str = "contatos.txt";
const SEPARATOR: char = '\x1F'; // Unit separator

// This is the closest to a global variable I could get.
thread_local!(static CONTATOS: RefCell<Vec<Contato>> = RefCell::new(vec![]));

/*
fn main_local_var() {
    //let mut contatos = vec![Contato{nome: "a".to_string(), telefone: "1234-5678".to_string()}];
    let mut contatos = vec![];
    //load
    while menu(&mut contatos) {
    }
    //save
}
*/

fn main_thread_local_var() {
    CONTATOS.with(|contatos: &RefCell<_>| {
        let contatos_mut = &mut *contatos.borrow_mut(); // Also works without the * because the result derefs automatically
        load(contatos_mut);
        menu(contatos_mut);
        save(contatos_mut);
    });
}

fn main() {
    main_thread_local_var()
}

fn load(contatos: &mut Vec<Contato>) {
    match File::open(FILE_NAME) {
        Ok(file) => {
            let reader = BufReader::new(file);
            for line in reader.lines() {
                let line = line.expect("Erro ao ler a linha");
                //println!("Leu: {}", &line);
                let split = line.split(SEPARATOR);
                let fields = split.collect::<Vec<&str>>();
                let n = fields[0];
                let t = fields[1];
                contatos.push(Contato{nome: String::from(n), telefone: String::from(t)});
            }
        },
        Err(_) => {},
    }
}

fn save(contatos: &mut Vec<Contato>) {
    match File::create(FILE_NAME) {
        Ok(mut file) => {
            for contato in contatos {
                file.write_fmt(format_args!("{}{}{}\r\n", contato.nome, SEPARATOR, contato.telefone))
                    .expect("Erro ao gravar contato");
            }
        },
        Err(e) => println!("Erro ao salvar o arquivo: {}", e),
    }
}

fn menu(contatos: &mut Vec<Contato>) {
    let menu_message = "
    === Menu ===
    (1) Inserir contato
    (2) Listar todos os contatos
    (3) Procurar contato por nome
    (4) Remover contato
    (5) Sair";
    loop {
        println!("{}", menu_message);
        let opcao = le_numero("Digite uma opção: ");
        match opcao {
            Ok(1) => inserir_contato(contatos),
            Ok(2) => listar_contatos(contatos),
            Ok(3) => procurar_contato(contatos),
            Ok(4) => remover_contato(contatos),
            Ok(5) => break,
            Ok(_) => println!("Opção inexistente"),
            Err(e) => println!("Por favor, digite um número ({})", e),
        }
    }
}

fn le_string(prompt: &str) -> String {
    print!("{}", prompt);
    io::stdout().flush().expect("Erro no flush de stdout");
    let mut line = String::new();
    io::stdin()
        .read_line(&mut line)
        .expect("Failed to read line");
    line = line.trim().to_owned(); // or: line = String::from(line.trim());
    line
}

fn le_numero(prompt: &str) -> Result<i32, <i32 as FromStr>::Err> {
    le_string(prompt).parse()
}

fn inserir_contato(contatos: &mut Vec<Contato>) {
    let n = le_string("Nome: ");
    let t = le_string("Telefone: ");
    contatos.push(Contato{nome: n, telefone: t});
}

fn listar_contatos(contatos: &Vec<Contato>) {
    for contato in contatos {
        imprimir_contato(contato);
    }
}

fn imprimir_contato(contato: &Contato) {
    println!("Nome: {}\nTelefone: {}\n", contato.nome, contato.telefone);
}

fn procurar_contato(contatos: &Vec<Contato>) {
    let nome = le_string("Nome procurado: ");
    for contato in contatos {
        if contato.nome == nome {
            imprimir_contato(contato);
            return;
        } else {
            //println!("«{}» é diferente de «{}», continuando a busca...", contato.nome, nome);
        }
    }
    println!("Contato não encontrado!");
}

fn remover_contato(contatos: &mut Vec<Contato>) {
    let nome_procurado = le_string("Nome a remover: ");
    for i in 0 .. contatos.len() {
        if {
            let contato = &contatos[i];
            let nome_tmp = &contato.nome;
            nome_tmp == &nome_procurado
        } {
            contatos.swap_remove(i);
            return;
        }
    }
}

