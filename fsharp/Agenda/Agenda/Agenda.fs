﻿module Agenda.MainModule

open System
open System.IO

type Contato = 
    { nome : string
      telefone : string }
    
    static member Novo(novoNome, novoTelefone) = 
        { nome = novoNome
          telefone = novoTelefone }
    
    member contato.Imprime() = 
        printfn "Nome: %s" contato.nome
        printfn "Telefone: %s" contato.telefone
        printfn ""

module KeyboardInput =
    let lêString prompt =
        printf "%s" prompt
        Console.ReadLine()

    let lêNúmero prompt =
        int <| lêString prompt


module DataStructure =
    let contatos = new ResizeArray<Contato>() // Exercício: usar Set e/ou (Functional) Array (já List não faço questão)

module UserInterface =
    let listarContatos() =
        for contato in DataStructure.contatos do
            contato.Imprime()

    let inserirContato() = 
        let nome = KeyboardInput.lêString "Nome: "
        let telefone = KeyboardInput.lêString "Telefone: "
        Contato.Novo(nome, telefone) |> DataStructure.contatos.Add

    let procurarContato() =
        let nome = KeyboardInput.lêString "Nome procurado: "
        match DataStructure.contatos |> Seq.tryFind (fun contato -> contato.nome = nome) with
            | Some(encontrado) -> encontrado.Imprime()
            | None -> printfn "Contato não encontrado!"

    let removerContato() =
        let nome = KeyboardInput.lêString "Nome a remover: "
        DataStructure.contatos.RemoveAll(fun contato -> contato.nome = nome) |> ignore
        ()

    let menu() =
        printfn """=== Menu ===
        (1) Inserir contato
        (2) Listar todos os contatos
        (3) Procurar contato por nome
        (4) Remover contato
        (5) Sair"""
        let opção = KeyboardInput.lêNúmero "Digite uma opção: "
        match opção with
        | 1 -> inserirContato(); true
        | 2 -> listarContatos(); true
        | 3 -> procurarContato(); true
        | 4 -> removerContato(); true
        | 5 -> false
        | _ -> true

module FileHandling =
    let fileName = "contatos.txt"
    let separator = '\x1F' // Unit separator

    let load() = 
        if File.Exists(fileName) then 
            let lines = File.ReadAllLines(fileName)
            for line in lines do
                let fields = line.Split(separator)
                let nome = fields.[0]
                let telefone = fields.[1]
                DataStructure.contatos.Add(Contato.Novo(nome, telefone))

    let save() =
        //use file = new FileStream(fileName, FileMode.Create)
        use file = new StreamWriter(fileName, append = false, encoding = Text.UTF8Encoding())
        for contato in DataStructure.contatos do
            let line = sprintf "%s%c%s\r\n" (contato.nome) separator (contato.telefone)
            file.Write line
    
let rec mainLoop() = 
    if UserInterface.menu() then
        mainLoop()

[<EntryPoint>]
let main argv = 
    FileHandling.load()
    mainLoop()
    FileHandling.save()
    0 // return an integer exit code
